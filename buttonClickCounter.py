import tkinter as tk

class basicDemo:
    def __init__(self, mainWindow):
        self.mainWindow = mainWindow
        self.frame = tk.Frame()

        self.number = 0

        tk.Button(self.frame, text = "Say Hello", command=self.sayHello).pack()
        tk.Label(self.frame, text = str(self.number) + str(self.number)).pack()
        self.frame.pack()

    def sayHello(self):
        global number
        self.number = self.number + 1 
        print("number self " + str(self.number))
        self.number += 1
        print("number " + str(self.number))

def main(): 
    mainWindow = tk.Tk()
    mainWindow.title("Say Hello to you Program")
    app = basicDemo(mainWindow)
    mainWindow.mainloop()

if __name__ == '__main__':
    main()