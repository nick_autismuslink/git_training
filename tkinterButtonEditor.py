import tkinter as tk

class Demo1:
    def __init__(self, master):
        self.master = master
        self.frame = tk.Frame(self.master)
        
        self.buttonExample = tk.Button(self.frame, text = "Click me to turn black", width = 25, command = self.clickedButton)
        self.buttonExample.pack()
        self.frame.pack()


    def clickedButton(self):
        currentColorOfButton = self.buttonExample.cget("bg")
        if currentColorOfButton == "black":
            self.buttonExample.config(bg="white", fg="black", text="now it's white")
        else:
            self.buttonExample.config(bg="black", fg="white", text="now the same button is black")

def main(): 
    root = tk.Tk()
    app = Demo1(root)
    root.mainloop()

if __name__ == '__main__':
    main()







